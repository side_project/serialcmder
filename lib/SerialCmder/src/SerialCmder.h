/*
串列通訊命令
*/

#include <Arduino.h>

#ifndef SerialCmder_h
#define SerialCmder_h

#define CMD_RX_BUFFER 300
#define CMD_MAX_LEN 32
//#define DEBUG

typedef struct {
  byte cmd[CMD_MAX_LEN];
  byte len;
  void (*function)(const byte*);
} CmdFeatureCallback;

typedef struct {
  uint8_t readTimeout = 10;  // 超時（ms）
} SerialCmderOptions;

class SerialCmder {
 private:
  SerialCmderOptions options;  // 模組設定

  CmdFeatureCallback* cmdFeatureList;  // 所有命令特徵清單
  byte cmdCounter = 0;                 // 目前命令總數

  void (*notFoundHandler)(const byte* cmd);  // 沒有匹配命令時 callback

  byte buffer[CMD_RX_BUFFER];  // RX Buffer
  byte bufferPos = 0;          // 目前接收 index 位置

  uint32_t timer;  // 計時器

  void compareCmdFeatureList(
      const byte* bufferIn,
      byte bufferPosIn);  // 比較目前 buffer 與 cmdList 是否有相符命令

 public:
  SerialCmder();
  SerialCmder(uint16_t readTimeout);

  void begin();
  void loop();

  // 增加命令
  void addCmdFeature(const byte* cmd,
                     byte len,
                     void (*function)(const byte* cmd));
  void addCmdFeature(const char* charCmd,
                     byte len,
                     void (*function)(const byte* cmd));

  // 輸入命令比對 cmdList 是否有相符命令
  void inputCmd(const byte* cmd, byte featureLen);

  // 設定查無命令時 Callback
  void setNotFoundHandler(void (*function)(const byte* cmd));

  // 清空 RX buffer
  void clearBuffer();
};

#endif
