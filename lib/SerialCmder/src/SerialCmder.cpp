#include "SerialCmder.h"

SerialCmder::SerialCmder() {}

SerialCmder::SerialCmder(uint16_t readTimeout) {
  options.readTimeout = readTimeout;
}

// --- private ---

/** 比較目前 buffer 與 cmdFeatureList 是否有相符命令
 */
void SerialCmder::compareCmdFeatureList(const byte* bufferIn,
                                        byte bufferPosIn) {
#ifdef DEBUG
  Serial.print(F("[ compareCmdFeatureList ] bufferPosIn : "));
  Serial.println(bufferPosIn);
#endif

  for (byte i = 0; i < cmdCounter; i++) {
    byte cmdLen = cmdFeatureList[i].len;
    // 目前接收長度不等於命令長度，下個命令
    if (bufferPosIn < cmdLen)
      continue;

    // 檢查設定命令部分是否與數值部分完全相符
    bool isMatch = true;
    for (byte cmdPos = 0; cmdPos < cmdLen; cmdPos++) {
#ifdef DEBUG
      Serial.print(F("[ compareCmdFeatureList ] cmd["));
      Serial.print(cmdPos);
      Serial.print(F("] : "));
      Serial.println(cmdFeatureList[i].cmd[cmdPos]);

      Serial.print(F("[ compareCmdFeatureList ] bufferIn["));
      Serial.print(cmdPos);
      Serial.print(F("] : "));
      Serial.println(bufferIn[cmdPos]);
#endif

      isMatch &= (cmdFeatureList[i].cmd[cmdPos] == bufferIn[cmdPos]);
      if (!isMatch)  // 不相符，跳出
        break;
    }

    if (!isMatch)  // 不完全相符，跳出，檢查下一個命令
      continue;

    // 完全相符，執行 callback
    (*cmdFeatureList[i].function)(bufferIn);
    clearBuffer();
    return;
  }

  // 查無任何相符命令
  if (notFoundHandler) {
    notFoundHandler(bufferIn);
  }
  clearBuffer();
}

// --- public ---

/** 開始初始化
 */
void SerialCmder::begin() {}

/** 需要在 main.cpp 呼叫，才有作用
 */
void SerialCmder::loop() {
  // 接收到命令時，開始計時
  if (bufferPos != 0) {
    if (millis() - timer >= options.readTimeout) {
      compareCmdFeatureList(buffer, bufferPos);
      timer = millis();
    }
  } else  // 沒有任何命令
  {
    timer = millis();
  }

  // 沒有任何數值
  if (Serial.available() <= 0) {
    return;
  }

  byte inByte = Serial.read();
  timer = millis();  // 接收到數值，更新計時器
#ifdef DEBUG
  Serial.printf_P(F("[ loop ] inByte : %X\n"), inByte);
#endif

  buffer[bufferPos] = inByte;
  bufferPos++;
}

/** 增加命令
 */
void SerialCmder::addCmdFeature(const byte* cmd,
                                byte len,
                                void (*function)(const byte* cmd)) {
  cmdFeatureList = (CmdFeatureCallback*)realloc(
      cmdFeatureList, (cmdCounter + 1) * sizeof(CmdFeatureCallback));

  memcpy(cmdFeatureList[cmdCounter].cmd, cmd, sizeof(cmd[0]) * len);
  cmdFeatureList[cmdCounter].function = function;
  cmdFeatureList[cmdCounter].len = len;

  cmdCounter++;
}

/** 增加 char 命令
 */
void SerialCmder::addCmdFeature(const char* cmd,
                                byte len,
                                void (*function)(const byte* cmd)) {
  cmdFeatureList = (CmdFeatureCallback*)realloc(
      cmdFeatureList, (cmdCounter + 1) * sizeof(CmdFeatureCallback));

  memcpy(cmdFeatureList[cmdCounter].cmd, cmd, sizeof(cmd[0]) * len);
  cmdFeatureList[cmdCounter].function = function;
  cmdFeatureList[cmdCounter].len = len;

  cmdCounter++;
}

/** 輸入命令比對 cmdList 是否有相符命令
 */
void SerialCmder::inputCmd(const byte* cmd, byte featureLen) {
  compareCmdFeatureList(cmd, featureLen);
}

/** 設定 cmd 比對失敗時的 callback
 */
void SerialCmder::setNotFoundHandler(void (*function)(const byte* cmd)) {
  notFoundHandler = function;
}

/** 清空 Buffer
 */
void SerialCmder::clearBuffer() {
  bufferPos = 0;
}
