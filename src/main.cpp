#include <Arduino.h>

#include "SerialCmder.h"

SerialCmder cmder;

// --- cmd ---
void handleCmd01(const byte* cmd) {
  Serial.printf_P(PSTR("[ handleCmd01 ]\n"));
}
void handleCmd02(const byte* cmd) {
  Serial.printf_P(PSTR("[ handleCmd02 ]\n"));
  byte inputCmd[] = {0xFA, 0x05, 0x05};
  cmder.inputCmd(inputCmd, 3);
}
void handleCmd03(const byte* cmd) {
  Serial.printf_P(PSTR("[ handleCmd03 ]\n"));
}

// --- charCmd ---
void handleCharCmd01(const byte* cmd) {
  Serial.printf_P(PSTR("[ handleCharCmd01 ]\n"));
}

void handleChar(const byte* cmd) {
  Serial.printf_P(PSTR("[ handleChar ]\n"));
}

// --- NotFound ---
void handleNotFound(const byte* cmd) {
  Serial.printf_P(PSTR("[ handleNotFound ]\n"));
}

// --- main ---

void setup() {
  Serial.begin(115200);
  Serial.printf_P(PSTR("[ boot ]"));

  cmder.begin();

  byte cmd01[] = {0xFA, 0x00, 0x02};
  cmder.addCmdFeature(cmd01, 3, handleCmd01);

  byte cmd02[] = {0xFA, 0x01, 0x01};
  cmder.addCmdFeature(cmd02, 3, handleCmd02);

  byte cmd03[] = {0xFA, 0x05, 0x05};
  cmder.addCmdFeature(cmd03, 3, handleCmd03);

  /**
   * 定義 char 命令特徵時，不可以用以下寫法
   * cmder.addCmdFeature("charCmd01", 9, handleCharCmd01);
   *
   * 命令特徵需要額外宣告變數，再以變數傳入，否則可能會發生不預期的異常。
   */
  char cmdChar01[] = "charCmd01";
  cmder.addCmdFeature(cmdChar01, 9, handleCharCmd01);

  char cmdChar[] = "char";
  cmder.addCmdFeature(cmdChar, 4, handleChar);

  cmder.setNotFoundHandler(handleNotFound);
}

void loop() {
  cmder.loop();
}